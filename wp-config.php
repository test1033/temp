<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vd]-%;cUJ?b|_Uwe5AK8`U:/%2p(C8%<T=Y{@~K,`qwv#9}e$Xle,uYj9ATg(J.2' );
define( 'SECURE_AUTH_KEY',  'q PkG3Fp/gIQqb[i=A>UflsDKVN[Np90Ni=`dng)OX;!z4IqpGQ3`du=mK}/PHr=' );
define( 'LOGGED_IN_KEY',    '}@ %/x5b^YKMn5n%*W:zV`#$u;y|{D)o.P*4YEaCO(fF3T5ZA4Pvoc>a1+jSEUI)' );
define( 'NONCE_KEY',        'u`_moE&Rm4Am(F@_N`8(06r>bSftoL!r(]-.9;Q`Ena[U/d3cNE_$#vTa@5TYgXw' );
define( 'AUTH_SALT',        'k_s6e9VJGbFq`QJS/UH/1`Wx3m`N~)WY=+9JfATF_^RKGejPm#Vt:gan=GL>ZYuf' );
define( 'SECURE_AUTH_SALT', ')bLGw-;=Ic4rYMLz_EWBjfTO1I~<MGn8$s)VsQ)Eidm5o-[iKH$`: y!.3%WzqZ>' );
define( 'LOGGED_IN_SALT',   'X9f3T $Imjv!D~3/S+Rx@6i71WL3$l&VrbwKUGB;)~yR(=oPD_F?9tIA2;Z$7Pff' );
define( 'NONCE_SALT',       'I4Az[9u696F|hO,:*idS>@22&p#tRzIug~1rEY8W?zQy4XTJ{{wz~Eg,eW9;8J`{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
reqire_once(ABSPATH . 'test');